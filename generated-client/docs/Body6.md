
# Body6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**admin** | **Boolean** |  |  [optional]
**language** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**password** | **String** |  |  [optional]



