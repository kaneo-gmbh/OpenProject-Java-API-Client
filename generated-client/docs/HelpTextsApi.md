# HelpTextsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3HelpTextsGet**](HelpTextsApi.md#apiV3HelpTextsGet) | **GET** /api/v3/help_texts | List all help texts
[**apiV3HelpTextsIdGet**](HelpTextsApi.md#apiV3HelpTextsIdGet) | **GET** /api/v3/help_texts/{id} | View help text


<a name="apiV3HelpTextsGet"></a>
# **apiV3HelpTextsGet**
> apiV3HelpTextsGet()

List all help texts



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.HelpTextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

HelpTextsApi apiInstance = new HelpTextsApi();
try {
    apiInstance.apiV3HelpTextsGet();
} catch (ApiException e) {
    System.err.println("Exception when calling HelpTextsApi#apiV3HelpTextsGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="apiV3HelpTextsIdGet"></a>
# **apiV3HelpTextsIdGet**
> apiV3HelpTextsIdGet(id)

View help text



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.HelpTextsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

HelpTextsApi apiInstance = new HelpTextsApi();
Integer id = 56; // Integer | Help text id
try {
    apiInstance.apiV3HelpTextsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling HelpTextsApi#apiV3HelpTextsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Help text id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

