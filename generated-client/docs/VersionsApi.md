# VersionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ProjectsProjectIdVersionsGet**](VersionsApi.md#apiV3ProjectsProjectIdVersionsGet) | **GET** /api/v3/projects/{project_id}/versions | List versions available in a project
[**apiV3VersionsGet**](VersionsApi.md#apiV3VersionsGet) | **GET** /api/v3/versions | List versions
[**apiV3VersionsIdGet**](VersionsApi.md#apiV3VersionsIdGet) | **GET** /api/v3/versions/{id} | View version


<a name="apiV3ProjectsProjectIdVersionsGet"></a>
# **apiV3ProjectsProjectIdVersionsGet**
> Versions apiV3ProjectsProjectIdVersionsGet(projectId)

List versions available in a project

This endpoint lists the versions that are *available* in a given project. Note that due to sharing this might be more than the versions *defined* by that project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.VersionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

VersionsApi apiInstance = new VersionsApi();
Integer projectId = 56; // Integer | ID of the project whoose versions will be listed
try {
    Versions result = apiInstance.apiV3ProjectsProjectIdVersionsGet(projectId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV3ProjectsProjectIdVersionsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**| ID of the project whoose versions will be listed |

### Return type

[**Versions**](Versions.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3VersionsGet"></a>
# **apiV3VersionsGet**
> Versions apiV3VersionsGet(filters)

List versions

Returns a collection of versions. The client can choose to filter the versions similar to how work packages are filtered. In addition to the provided filters, the server will reduce the result set to only contain versions, for which the requesting client has sufficient permissions (*view_work_packages*).

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.VersionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

VersionsApi apiInstance = new VersionsApi();
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + sharing: filters versions by how they are shared within the server (*none*, *descendants*, *hierarchy*, *tree*, *system*).
try {
    Versions result = apiInstance.apiV3VersionsGet(filters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV3VersionsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + sharing: filters versions by how they are shared within the server (*none*, *descendants*, *hierarchy*, *tree*, *system*). | [optional]

### Return type

[**Versions**](Versions.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3VersionsIdGet"></a>
# **apiV3VersionsIdGet**
> Version apiV3VersionsIdGet(id)

View version



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.VersionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

VersionsApi apiInstance = new VersionsApi();
Integer id = 56; // Integer | version id
try {
    Version result = apiInstance.apiV3VersionsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV3VersionsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| version id |

### Return type

[**Version**](Version.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

