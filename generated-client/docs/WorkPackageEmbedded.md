
# WorkPackageEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**WorkPackage**](WorkPackage.md) |  |  [optional]
**project** | [**Project**](Project.md) |  |  [optional]
**type** | [**WPType**](WPType.md) |  |  [optional]
**version** | [**Version**](Version.md) |  |  [optional]



