
# TimeEntryLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | [**Link**](Link.md) |  |  [optional]
**project** | [**Link**](Link.md) |  |  [optional]
**workPackage** | [**Link**](Link.md) |  |  [optional]
**user** | [**Link**](Link.md) |  |  [optional]
**activity** | [**Link**](Link.md) |  |  [optional]



