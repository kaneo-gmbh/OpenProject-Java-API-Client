# StatusesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3StatusesGet**](StatusesApi.md#apiV3StatusesGet) | **GET** /api/v3/statuses | List all Statuses
[**apiV3StatusesIdGet**](StatusesApi.md#apiV3StatusesIdGet) | **GET** /api/v3/statuses/{id} | View Status


<a name="apiV3StatusesGet"></a>
# **apiV3StatusesGet**
> apiV3StatusesGet()

List all Statuses



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.StatusesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

StatusesApi apiInstance = new StatusesApi();
try {
    apiInstance.apiV3StatusesGet();
} catch (ApiException e) {
    System.err.println("Exception when calling StatusesApi#apiV3StatusesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3StatusesIdGet"></a>
# **apiV3StatusesIdGet**
> apiV3StatusesIdGet(id)

View Status



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.StatusesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

StatusesApi apiInstance = new StatusesApi();
Integer id = 56; // Integer | status id
try {
    apiInstance.apiV3StatusesIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusesApi#apiV3StatusesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| status id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

