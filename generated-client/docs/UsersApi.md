# UsersApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3UsersGet**](UsersApi.md#apiV3UsersGet) | **GET** /api/v3/users | List Users
[**apiV3UsersIdDelete**](UsersApi.md#apiV3UsersIdDelete) | **DELETE** /api/v3/users/{id} | Delete user
[**apiV3UsersIdGet**](UsersApi.md#apiV3UsersIdGet) | **GET** /api/v3/users/{id} | View user
[**apiV3UsersIdLockDelete**](UsersApi.md#apiV3UsersIdLockDelete) | **DELETE** /api/v3/users/{id}/lock | Remove Lock
[**apiV3UsersIdLockPost**](UsersApi.md#apiV3UsersIdLockPost) | **POST** /api/v3/users/{id}/lock | Set Lock
[**apiV3UsersIdPatch**](UsersApi.md#apiV3UsersIdPatch) | **PATCH** /api/v3/users/{id} | Update user
[**apiV3UsersPost**](UsersApi.md#apiV3UsersPost) | **POST** /api/v3/users | Create User


<a name="apiV3UsersGet"></a>
# **apiV3UsersGet**
> Users apiV3UsersGet(offset, pageSize, filters, sortBy)

List Users

Lists users. Only administrators have permission to do this.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Integer offset = 1; // Integer | Page number inside the requested collection.
Integer pageSize = 56; // Integer | Number of elements to display per page.
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + status: Status the user has  + group: Name of the group in which to-be-listed users are members.  + name: Filter users in whose first or last names, or email addresses the given string occurs.  + login: User's login
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint.
try {
    Users result = apiInstance.apiV3UsersGet(offset, pageSize, filters, sortBy);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **Integer**| Page number inside the requested collection. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page. | [optional]
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + status: Status the user has  + group: Name of the group in which to-be-listed users are members.  + name: Filter users in whose first or last names, or email addresses the given string occurs.  + login: User&#39;s login | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]

### Return type

[**Users**](Users.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersIdDelete"></a>
# **apiV3UsersIdDelete**
> apiV3UsersIdDelete(id)

Delete user

Permanently deletes the specified user account.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | User id
try {
    apiInstance.apiV3UsersIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| User id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersIdGet"></a>
# **apiV3UsersIdGet**
> User apiV3UsersIdGet(id)

View user



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
String id = "id_example"; // String | User id. Use `me` to reference current user, if any.
try {
    User result = apiInstance.apiV3UsersIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| User id. Use &#x60;me&#x60; to reference current user, if any. |

### Return type

[**User**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersIdLockDelete"></a>
# **apiV3UsersIdLockDelete**
> apiV3UsersIdLockDelete(id)

Remove Lock



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | User id
try {
    apiInstance.apiV3UsersIdLockDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersIdLockDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| User id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersIdLockPost"></a>
# **apiV3UsersIdLockPost**
> apiV3UsersIdLockPost(id)

Set Lock



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | User id
try {
    apiInstance.apiV3UsersIdLockPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersIdLockPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| User id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersIdPatch"></a>
# **apiV3UsersIdPatch**
> apiV3UsersIdPatch(id, body)

Update user

Updates the user&#39;s writable attributes. When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | User id
Body5 body = new Body5(); // Body5 | 
try {
    apiInstance.apiV3UsersIdPatch(id, body);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersIdPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| User id |
 **body** | [**Body5**](Body5.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3UsersPost"></a>
# **apiV3UsersPost**
> apiV3UsersPost(body)

Create User

Creates a new user. Only administrators have permission to do so. When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body.  Valid values for &#x60;status&#x60;:  1) \&quot;active\&quot; - In this case a password has to be provided in addition to the other attributes. 2) \&quot;invited\&quot; - In this case nothing but the email address is required. The rest is optional. An invitation will be sent to the user.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UsersApi apiInstance = new UsersApi();
Body6 body = new Body6(); // Body6 | 
try {
    apiInstance.apiV3UsersPost(body);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#apiV3UsersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body6**](Body6.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

