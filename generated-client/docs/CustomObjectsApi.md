# CustomObjectsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3CustomObjectsIdGet**](CustomObjectsApi.md#apiV3CustomObjectsIdGet) | **GET** /api/v3/custom_objects/{id} | View Custom Object


<a name="apiV3CustomObjectsIdGet"></a>
# **apiV3CustomObjectsIdGet**
> apiV3CustomObjectsIdGet(id)

View Custom Object



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CustomObjectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CustomObjectsApi apiInstance = new CustomObjectsApi();
Integer id = 56; // Integer | The custom object's identifier
try {
    apiInstance.apiV3CustomObjectsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomObjectsApi#apiV3CustomObjectsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The custom object&#39;s identifier |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

