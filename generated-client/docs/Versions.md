
# Versions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**VersionsEmbedded**](VersionsEmbedded.md) |  |  [optional]



