# QuerySortBysApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3QueriesSortBysIdGet**](QuerySortBysApi.md#apiV3QueriesSortBysIdGet) | **GET** /api/v3/queries/sort_bys/{id} | View Query Sort By


<a name="apiV3QueriesSortBysIdGet"></a>
# **apiV3QueriesSortBysIdGet**
> apiV3QueriesSortBysIdGet(id)

View Query Sort By

Retreive an individual QuerySortBy as identified by the id parameter.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QuerySortBysApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QuerySortBysApi apiInstance = new QuerySortBysApi();
String id = "id_example"; // String | QuerySortBy identifier. The identifier is a combination of the column identifier and the direction.
try {
    apiInstance.apiV3QueriesSortBysIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QuerySortBysApi#apiV3QueriesSortBysIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| QuerySortBy identifier. The identifier is a combination of the column identifier and the direction. |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

