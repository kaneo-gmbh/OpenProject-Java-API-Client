
# Users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**UsersEmbedded**](UsersEmbedded.md) |  |  [optional]



