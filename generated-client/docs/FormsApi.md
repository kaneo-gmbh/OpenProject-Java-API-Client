# FormsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ExampleFormPost**](FormsApi.md#apiV3ExampleFormPost) | **POST** /api/v3/example/form | show or validate form


<a name="apiV3ExampleFormPost"></a>
# **apiV3ExampleFormPost**
> apiV3ExampleFormPost(body)

show or validate form

This is an example of how a form might look like. Note that this endpoint does not exist in the actual implementation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.FormsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

FormsApi apiInstance = new FormsApi();
Body2 body = new Body2(); // Body2 | 
try {
    apiInstance.apiV3ExampleFormPost(body);
} catch (ApiException e) {
    System.err.println("Exception when calling FormsApi#apiV3ExampleFormPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body2**](Body2.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

