# StringObjectsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3StringObjectsGet**](StringObjectsApi.md#apiV3StringObjectsGet) | **GET** /api/v3/string_objects | View String Object


<a name="apiV3StringObjectsGet"></a>
# **apiV3StringObjectsGet**
> apiV3StringObjectsGet(value)

View String Object



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.StringObjectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

StringObjectsApi apiInstance = new StringObjectsApi();
String value = "value_example"; // String | The string value being resolved
try {
    apiInstance.apiV3StringObjectsGet(value);
} catch (ApiException e) {
    System.err.println("Exception when calling StringObjectsApi#apiV3StringObjectsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | **String**| The string value being resolved |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

