# RelationsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3RelationsGet**](RelationsApi.md#apiV3RelationsGet) | **GET** /api/v3/relations | List Relations
[**apiV3RelationsIdDelete**](RelationsApi.md#apiV3RelationsIdDelete) | **DELETE** /api/v3/relations/{id} | Delete Relation
[**apiV3RelationsIdFormPost**](RelationsApi.md#apiV3RelationsIdFormPost) | **POST** /api/v3/relations/{id}/form | Relation edit form
[**apiV3RelationsIdGet**](RelationsApi.md#apiV3RelationsIdGet) | **GET** /api/v3/relations/{id} | View Relation
[**apiV3RelationsIdPatch**](RelationsApi.md#apiV3RelationsIdPatch) | **PATCH** /api/v3/relations/{id} | Edit Relation
[**apiV3RelationsSchemaGet**](RelationsApi.md#apiV3RelationsSchemaGet) | **GET** /api/v3/relations/schema | View relation schema
[**apiV3RelationsSchemaTypeGet**](RelationsApi.md#apiV3RelationsSchemaTypeGet) | **GET** /api/v3/relations/schema/{type} | View relation schema for type


<a name="apiV3RelationsGet"></a>
# **apiV3RelationsGet**
> apiV3RelationsGet(filters, sortBy)

List Relations

Lists all relations according to the given (optional, logically conjunctive) filters and ordered by ID. The response only includes relations between work packages which the user is allowed to see.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Valid fields to filter by are:  + id - ID of relation  + from - ID of work package from which the filtered relations emanates.  + to - ID of work package to which this related points.  + involved - ID of either the `from` or the `to` work package.  + type - The type of relation to filter by, e.g. \"follows\".
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint.
try {
    apiInstance.apiV3RelationsGet(filters, sortBy);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Valid fields to filter by are:  + id - ID of relation  + from - ID of work package from which the filtered relations emanates.  + to - ID of work package to which this related points.  + involved - ID of either the &#x60;from&#x60; or the &#x60;to&#x60; work package.  + type - The type of relation to filter by, e.g. \&quot;follows\&quot;. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsIdDelete"></a>
# **apiV3RelationsIdDelete**
> apiV3RelationsIdDelete(id)

Delete Relation

Deletes the relation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
Integer id = 56; // Integer | Relation ID
try {
    apiInstance.apiV3RelationsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Relation ID |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsIdFormPost"></a>
# **apiV3RelationsIdFormPost**
> apiV3RelationsIdFormPost(id)

Relation edit form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
Integer id = 56; // Integer | ID of the relation being modified
try {
    apiInstance.apiV3RelationsIdFormPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsIdFormPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the relation being modified |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsIdGet"></a>
# **apiV3RelationsIdGet**
> apiV3RelationsIdGet(id)

View Relation



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
Integer id = 56; // Integer | Relation id
try {
    apiInstance.apiV3RelationsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Relation id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsIdPatch"></a>
# **apiV3RelationsIdPatch**
> apiV3RelationsIdPatch(id)

Edit Relation

When calling this endpoint the client provides a single object, containing the properties and links that it wants to change, in the body. It is only allowed to provide properties or links supporting the **write** operation.  Note that changing the &#x60;type&#x60; of a relation invariably also changes the respective &#x60;reverseType&#x60; as well as the \&quot;name\&quot; of it. The returned Relation object will reflect that change. For instance if you change a Relation&#39;s &#x60;type&#x60; to \&quot;follows\&quot; then the &#x60;reverseType&#x60; will be changed to &#x60;precedes&#x60;.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
Integer id = 56; // Integer | Relation ID
try {
    apiInstance.apiV3RelationsIdPatch(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsIdPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Relation ID |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsSchemaGet"></a>
# **apiV3RelationsSchemaGet**
> apiV3RelationsSchemaGet()

View relation schema



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
try {
    apiInstance.apiV3RelationsSchemaGet();
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsSchemaGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RelationsSchemaTypeGet"></a>
# **apiV3RelationsSchemaTypeGet**
> apiV3RelationsSchemaTypeGet(type)

View relation schema for type



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RelationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RelationsApi apiInstance = new RelationsApi();
String type = "type_example"; // String | Type of the schema
try {
    apiInstance.apiV3RelationsSchemaTypeGet(type);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationsApi#apiV3RelationsSchemaTypeGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| Type of the schema |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

