# CustomActionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3CustomActionsCustomActionIdExecutePost**](CustomActionsApi.md#apiV3CustomActionsCustomActionIdExecutePost) | **POST** /api/v3/custom_actions/{custom_action_id}/execute | Execute custom action
[**apiV3CustomActionsCustomActionIdGet**](CustomActionsApi.md#apiV3CustomActionsCustomActionIdGet) | **GET** /api/v3/custom_actions/{custom_action_id} | View custom action


<a name="apiV3CustomActionsCustomActionIdExecutePost"></a>
# **apiV3CustomActionsCustomActionIdExecutePost**
> apiV3CustomActionsCustomActionIdExecutePost(customActionId, body)

Execute custom action

A POST to this end point executes the custom action on the work package provided in the payload. The altered work package will be returned. In order to avoid executing  the custom action unbeknown to a change that has already taken place, the client has to provide the work package&#39;s current lockVersion.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CustomActionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CustomActionsApi apiInstance = new CustomActionsApi();
Integer customActionId = 56; // Integer | The id of the custom action to execute
Body1 body = new Body1(); // Body1 | 
try {
    apiInstance.apiV3CustomActionsCustomActionIdExecutePost(customActionId, body);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomActionsApi#apiV3CustomActionsCustomActionIdExecutePost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customActionId** | **Integer**| The id of the custom action to execute |
 **body** | [**Body1**](Body1.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3CustomActionsCustomActionIdGet"></a>
# **apiV3CustomActionsCustomActionIdGet**
> apiV3CustomActionsCustomActionIdGet(customActionId)

View custom action



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CustomActionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CustomActionsApi apiInstance = new CustomActionsApi();
Integer customActionId = 56; // Integer | The id of the custom action to fetch
try {
    apiInstance.apiV3CustomActionsCustomActionIdGet(customActionId);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomActionsApi#apiV3CustomActionsCustomActionIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customActionId** | **Integer**| The id of the custom action to fetch |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

