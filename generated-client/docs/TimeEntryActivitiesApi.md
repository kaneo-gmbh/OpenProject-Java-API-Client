# TimeEntryActivitiesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3TimeEntriesActivityIdGet**](TimeEntryActivitiesApi.md#apiV3TimeEntriesActivityIdGet) | **GET** /api/v3/time_entries/activity/{id} | View time entries activity


<a name="apiV3TimeEntriesActivityIdGet"></a>
# **apiV3TimeEntriesActivityIdGet**
> apiV3TimeEntriesActivityIdGet(id)

View time entries activity



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TimeEntryActivitiesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TimeEntryActivitiesApi apiInstance = new TimeEntryActivitiesApi();
Integer id = 56; // Integer | time entries activity id
try {
    apiInstance.apiV3TimeEntriesActivityIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeEntryActivitiesApi#apiV3TimeEntriesActivityIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| time entries activity id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

