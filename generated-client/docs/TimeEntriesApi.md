# TimeEntriesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3TimeEntriesGet**](TimeEntriesApi.md#apiV3TimeEntriesGet) | **GET** /api/v3/time_entries | List Time entries
[**apiV3TimeEntriesIdGet**](TimeEntriesApi.md#apiV3TimeEntriesIdGet) | **GET** /api/v3/time_entries/{id} | View time entry


<a name="apiV3TimeEntriesGet"></a>
# **apiV3TimeEntriesGet**
> TimeEntries apiV3TimeEntriesGet(offset, pageSize, filters)

List Time entries

Lists time entries. The time entries returned depend on the filters provided and also on the permission of the requesting user.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TimeEntriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TimeEntriesApi apiInstance = new TimeEntriesApi();
Integer offset = 1; // Integer | Page number inside the requested collection.
Integer pageSize = 56; // Integer | Number of elements to display per page.
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + work_package: Filter time entries by work package  + project: Filter time entries by project  + user: Filter time entries by users
try {
    TimeEntries result = apiInstance.apiV3TimeEntriesGet(offset, pageSize, filters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeEntriesApi#apiV3TimeEntriesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **Integer**| Page number inside the requested collection. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page. | [optional]
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + work_package: Filter time entries by work package  + project: Filter time entries by project  + user: Filter time entries by users | [optional]

### Return type

[**TimeEntries**](TimeEntries.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3TimeEntriesIdGet"></a>
# **apiV3TimeEntriesIdGet**
> TimeEntry apiV3TimeEntriesIdGet(id)

View time entry



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TimeEntriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TimeEntriesApi apiInstance = new TimeEntriesApi();
Integer id = 56; // Integer | time entry id
try {
    TimeEntry result = apiInstance.apiV3TimeEntriesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TimeEntriesApi#apiV3TimeEntriesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| time entry id |

### Return type

[**TimeEntry**](TimeEntry.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

