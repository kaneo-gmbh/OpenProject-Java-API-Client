
# TimeEntriesLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | [**Link**](Link.md) |  |  [optional]
**jumpTo** | [**Link**](Link.md) |  |  [optional]
**changeSize** | [**Link**](Link.md) |  |  [optional]
**nextByOffset** | [**Link**](Link.md) |  |  [optional]



