# QueryFilterInstanceSchemaApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ProjectsIdQueriesFilterInstanceSchemasGet**](QueryFilterInstanceSchemaApi.md#apiV3ProjectsIdQueriesFilterInstanceSchemasGet) | **GET** /api/v3/projects/{id}/queries/filter_instance_schemas | List Query Filter Instance Schemas for Project
[**apiV3QueriesFilterInstanceSchemasGet**](QueryFilterInstanceSchemaApi.md#apiV3QueriesFilterInstanceSchemasGet) | **GET** /api/v3/queries/filter_instance_schemas | List Query Filter Instance Schemas
[**apiV3QueriesFilterInstanceSchemasIdentifierGet**](QueryFilterInstanceSchemaApi.md#apiV3QueriesFilterInstanceSchemasIdentifierGet) | **GET** /api/v3/queries/filter_instance_schemas/{identifier} | View Query Filter Instance Schema


<a name="apiV3ProjectsIdQueriesFilterInstanceSchemasGet"></a>
# **apiV3ProjectsIdQueriesFilterInstanceSchemasGet**
> apiV3ProjectsIdQueriesFilterInstanceSchemasGet(id)

List Query Filter Instance Schemas for Project

Returns the list of QueryFilterInstanceSchemas defined for a query of the specified project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryFilterInstanceSchemaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryFilterInstanceSchemaApi apiInstance = new QueryFilterInstanceSchemaApi();
Integer id = 56; // Integer | Id of the project.
try {
    apiInstance.apiV3ProjectsIdQueriesFilterInstanceSchemasGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryFilterInstanceSchemaApi#apiV3ProjectsIdQueriesFilterInstanceSchemasGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Id of the project. |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesFilterInstanceSchemasGet"></a>
# **apiV3QueriesFilterInstanceSchemasGet**
> apiV3QueriesFilterInstanceSchemasGet()

List Query Filter Instance Schemas

Returns the list of QueryFilterInstanceSchemas defined for a global query. That is a query not assigned to a project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryFilterInstanceSchemaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryFilterInstanceSchemaApi apiInstance = new QueryFilterInstanceSchemaApi();
try {
    apiInstance.apiV3QueriesFilterInstanceSchemasGet();
} catch (ApiException e) {
    System.err.println("Exception when calling QueryFilterInstanceSchemaApi#apiV3QueriesFilterInstanceSchemasGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesFilterInstanceSchemasIdentifierGet"></a>
# **apiV3QueriesFilterInstanceSchemasIdentifierGet**
> apiV3QueriesFilterInstanceSchemasIdentifierGet(identifier)

View Query Filter Instance Schema

Retreive an individual QueryFilterInstanceSchema as identified by the id parameter.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryFilterInstanceSchemaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryFilterInstanceSchemaApi apiInstance = new QueryFilterInstanceSchemaApi();
String identifier = "identifier_example"; // String | QueryFilterInstanceSchema identifier. The identifier is the filter identifier.
try {
    apiInstance.apiV3QueriesFilterInstanceSchemasIdentifierGet(identifier);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryFilterInstanceSchemaApi#apiV3QueriesFilterInstanceSchemasIdentifierGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **identifier** | **String**| QueryFilterInstanceSchema identifier. The identifier is the filter identifier. |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

