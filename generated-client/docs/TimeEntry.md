
# TimeEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**hours** | **String** |  |  [optional]
**comment** | **String** |  |  [optional]
**spentOn** | [**LocalDate**](LocalDate.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**embedded** | [**TimeEntryEmbedded**](TimeEntryEmbedded.md) |  |  [optional]
**links** | [**TimeEntryLinks**](TimeEntryLinks.md) |  |  [optional]



