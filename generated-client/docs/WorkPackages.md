
# WorkPackages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**WorkPackagesEmbedded**](WorkPackagesEmbedded.md) |  |  [optional]



