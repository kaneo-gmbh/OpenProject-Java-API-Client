/*
 * OpenProject API v3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.openproject.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import org.openproject.client.model.Description;
import org.openproject.client.model.WorkPackageEmbedded;
import org.openproject.client.model.WorkPackageLinks;

/**
 * WorkPackage
 */

public class WorkPackage {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("lockVersion")
  private Integer lockVersion = null;

  @SerializedName("percentageDone")
  private Integer percentageDone = null;

  @SerializedName("subject")
  private String subject = null;

  @SerializedName("description")
  private Description description = null;

  @SerializedName("startDate")
  private OffsetDateTime startDate = null;

  @SerializedName("dueDate")
  private OffsetDateTime dueDate = null;

  @SerializedName("createdAt")
  private OffsetDateTime createdAt = null;

  @SerializedName("updatedAt")
  private OffsetDateTime updatedAt = null;

  @SerializedName("estimatedTime")
  private String estimatedTime = null;

  @SerializedName("_embedded")
  private WorkPackageEmbedded embedded = null;

  @SerializedName("_links")
  private WorkPackageLinks links = null;

  public WorkPackage id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public WorkPackage lockVersion(Integer lockVersion) {
    this.lockVersion = lockVersion;
    return this;
  }

   /**
   * Get lockVersion
   * @return lockVersion
  **/
  @ApiModelProperty(value = "")
  public Integer getLockVersion() {
    return lockVersion;
  }

  public void setLockVersion(Integer lockVersion) {
    this.lockVersion = lockVersion;
  }

  public WorkPackage percentageDone(Integer percentageDone) {
    this.percentageDone = percentageDone;
    return this;
  }

   /**
   * Get percentageDone
   * @return percentageDone
  **/
  @ApiModelProperty(value = "")
  public Integer getPercentageDone() {
    return percentageDone;
  }

  public void setPercentageDone(Integer percentageDone) {
    this.percentageDone = percentageDone;
  }

  public WorkPackage subject(String subject) {
    this.subject = subject;
    return this;
  }

   /**
   * Get subject
   * @return subject
  **/
  @ApiModelProperty(value = "")
  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public WorkPackage description(Description description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public Description getDescription() {
    return description;
  }

  public void setDescription(Description description) {
    this.description = description;
  }

  public WorkPackage startDate(OffsetDateTime startDate) {
    this.startDate = startDate;
    return this;
  }

   /**
   * Get startDate
   * @return startDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(OffsetDateTime startDate) {
    this.startDate = startDate;
  }

  public WorkPackage dueDate(OffsetDateTime dueDate) {
    this.dueDate = dueDate;
    return this;
  }

   /**
   * Get dueDate
   * @return dueDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(OffsetDateTime dueDate) {
    this.dueDate = dueDate;
  }

  public WorkPackage createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public WorkPackage updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  public WorkPackage estimatedTime(String estimatedTime) {
    this.estimatedTime = estimatedTime;
    return this;
  }

   /**
   * Get estimatedTime
   * @return estimatedTime
  **/
  @ApiModelProperty(value = "")
  public String getEstimatedTime() {
    return estimatedTime;
  }

  public void setEstimatedTime(String estimatedTime) {
    this.estimatedTime = estimatedTime;
  }

  public WorkPackage embedded(WorkPackageEmbedded embedded) {
    this.embedded = embedded;
    return this;
  }

   /**
   * Get embedded
   * @return embedded
  **/
  @ApiModelProperty(value = "")
  public WorkPackageEmbedded getEmbedded() {
    return embedded;
  }

  public void setEmbedded(WorkPackageEmbedded embedded) {
    this.embedded = embedded;
  }

  public WorkPackage links(WorkPackageLinks links) {
    this.links = links;
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")
  public WorkPackageLinks getLinks() {
    return links;
  }

  public void setLinks(WorkPackageLinks links) {
    this.links = links;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorkPackage workPackage = (WorkPackage) o;
    return Objects.equals(this.id, workPackage.id) &&
        Objects.equals(this.lockVersion, workPackage.lockVersion) &&
        Objects.equals(this.percentageDone, workPackage.percentageDone) &&
        Objects.equals(this.subject, workPackage.subject) &&
        Objects.equals(this.description, workPackage.description) &&
        Objects.equals(this.startDate, workPackage.startDate) &&
        Objects.equals(this.dueDate, workPackage.dueDate) &&
        Objects.equals(this.createdAt, workPackage.createdAt) &&
        Objects.equals(this.updatedAt, workPackage.updatedAt) &&
        Objects.equals(this.estimatedTime, workPackage.estimatedTime) &&
        Objects.equals(this.embedded, workPackage.embedded) &&
        Objects.equals(this.links, workPackage.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, lockVersion, percentageDone, subject, description, startDate, dueDate, createdAt, updatedAt, estimatedTime, embedded, links);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WorkPackage {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lockVersion: ").append(toIndentedString(lockVersion)).append("\n");
    sb.append("    percentageDone: ").append(toIndentedString(percentageDone)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
    sb.append("    dueDate: ").append(toIndentedString(dueDate)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    estimatedTime: ").append(toIndentedString(estimatedTime)).append("\n");
    sb.append("    embedded: ").append(toIndentedString(embedded)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

