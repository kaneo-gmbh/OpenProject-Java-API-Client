/*
 * OpenProject API v3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.openproject.client.api;

import org.openproject.client.ApiException;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for TimeEntryActivitiesApi
 */
@Ignore
public class TimeEntryActivitiesApiTest {

    private final TimeEntryActivitiesApi api = new TimeEntryActivitiesApi();

    
    /**
     * View time entries activity
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void apiV3TimeEntriesActivityIdGetTest() throws ApiException {
        Integer id = null;
        api.apiV3TimeEntriesActivityIdGet(id);

        // TODO: test validations
    }
    
}
